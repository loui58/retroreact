 import React, {PureComponent} from 'react';
 import {Link} from 'react-router';

 export default class Welcome extends PureComponent{
    render(){
        return(
            <div className="inner cover">
                <h1 className="cover-heading">Halo</h1>
                <p className="lead">Klik aja, iseng2 doang</p>
                <p className="lead">
                    <Link className="btn btn-lg" to="/games">Cari</Link>
                </p>
            </div>
        );
    }
 
 }